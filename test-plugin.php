<?php
/**
 * Plugin Name: Test Plugin
 * Plugin URI: #
 * Description: Place [test_plugin_init button="button name"] shortcode on the page
 * Version: 0.1
 * Author: Nikita Dudarev
 * Author URI: #
 * Text Domain: text-plugin
 * Domain Path: /languages/
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly.
}

if ( ! defined( 'TESTPLUGIN_URL' ) ) {
    define( 'TESTPLUGIN_URL' , plugin_dir_url( __FILE__ ) );
}

if ( ! defined( 'TESTPLUGIN_PATH' ) ) {
    define( 'TESTPLUGIN_PATH' , plugin_dir_path( __FILE__ ) );
}

include_once TESTPLUGIN_PATH . 'includes/class-config.php';
include_once TESTPLUGIN_PATH . 'includes/class-test.php';