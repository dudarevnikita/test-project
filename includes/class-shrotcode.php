<?php
namespace App\Includes;

use App\Includes\TestConfig as Config;

if ( ! defined( 'ABSPATH' ) ) { 
    exit; // Exit if accessed directly
}

class TestShortcode
{

    public function __construct()
    {
        /**
         * Init shortcode
         */
        add_shortcode( 'test_plugin_init', array( $this, 'shortcode' ) );

        /**
         * test ajax event hendler
         */
        add_action( 'wp_ajax_test_ajax_plugin', array( $this, 'get_user_information' ) );
        add_action( 'wp_ajax_nopriv_test_ajax_plugin', array( $this, 'get_user_information' ) );

        /**
         * Includes styles and scripts
         */
        add_action( 'wp_enqueue_scripts', array( $this, 'init_styles_scripts' ) );
    }

    public function shortcode( $atts )
    {
        $atts = shortcode_atts( array(
            'button'   => 'Press',     
        ), $atts );

        return $this->display_shortcode( $atts );
    }

    public function display_shortcode( $atts )
    {
        Config::includeForm( 'test-tp-display-btn', $atts );
    }

    private function check_user()
    {
        if ( is_user_logged_in() ) {
            return get_current_user_id();
        } else {
            return false;
        }
    }

    public function get_user_information()
    {
        $user_id = ( ! empty( $_POST['user_id'] ) ? $_POST['user_id'] : false );

        if ( $user_id ) {
            $information = array(
                'information'   => true,
                'nickname'      => get_user_meta( $user_id, 'nickname', true ),
                'first_name'    => get_user_meta( $user_id, 'first_name', true ),
                'last_name'     => get_user_meta( $user_id, 'last_name', true )
            ); 
            Config::includeForm( 'test-tp-display-inf', $information );
            wp_die();
        } else {
            $information = array(
                'information'   => false
            ); 
            Config::includeForm( 'test-tp-display-inf', $information );
            wp_die();
        }
    }

    public function init_styles_scripts()
    {
        wp_enqueue_script( 
            'test_plugin_scripts',
            TESTPLUGIN_URL . 'assets/js/test-plugin-scripts.js',
            array('jquery'),
            '0.1',
            true
        );

        wp_enqueue_style(
            'test_plugin_styles',
            TESTPLUGIN_URL . 'assets/css/test-plugin-styles.css',
            false,
            '0.1'
        );

        wp_localize_script(
            'test_plugin_scripts',
            'testPluginVar',
            array( 'ajax_url' => admin_url( 'admin-ajax.php' ) , 'user_id' => $this->check_user() )
        );
    }

}

if ( class_exists( '\App\Includes\TestShortcode' ) ) {
    new TestShortcode();
}