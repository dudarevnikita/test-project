<?php
namespace App\Includes;

if ( ! defined( 'ABSPATH' ) ) { 
    exit; // Exit if accessed directly
}

class Test_Class
{

    public function __construct()
    {
        add_action( 'plugins_loaded', array( $this, 'plugin_loaded' ) );
    }

    public function plugin_loaded()
    {
        include_once TESTPLUGIN_PATH . '/includes/class-shrotcode.php';
    }

}

if ( class_exists( '\App\Includes\Test_Class' ) ) {
    $GLOBALS['test_class'] = new Test_Class();
}