<?php if ( $information ): ?>
    <p class="nickname"><strong><?php _e( 'Nickname:', 'text-plugin' ) ?></strong> <?php echo $nickname ?></p>
    <p class="first_name"><strong><?php _e( 'First Name:', 'text-plugin' ) ?></strong> <?php echo $first_name ?></p>
    <p class="last_name"><strong><?php _e( 'Last Name:', 'text-plugin' ) ?></strong> <?php echo $last_name ?></p>
<?php else: ?>
    <p class="error"><?php _e( 'Information not found! Please Sign In!', 'text-plugin' ) ?></p>
<?php endif; ?>