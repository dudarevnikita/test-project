;jQuery(document).ready(function (){
    
    jQuery('#wrap-test-plugin').on('click', 'button.test-btn', function(e){
        jQuery('.response').empty();

        var target = jQuery(e.target);

        jQuery(target).text('Wait...');
        jQuery(target).prop('disabled', true);

        jQuery.ajax({
                url: testPluginVar.ajax_url,
                method: 'POST',
                data: {
                    action: 'test_ajax_plugin',
                    user_id: testPluginVar.user_id
                }
            }).done(function(data){
                jQuery('.response').append(data);
                jQuery(target).text('Press');
                jQuery(target).prop('disabled', false);
            }).fail(function(){
                console.log("Error while request!");
            });
    });

});